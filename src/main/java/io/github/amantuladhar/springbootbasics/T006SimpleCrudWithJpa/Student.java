package io.github.amantuladhar.springbootbasics.T006SimpleCrudWithJpa;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Aman Tuladhar
 * @since 2018-01-11
 */

@Entity

@Data
@NoArgsConstructor
@AllArgsConstructor
class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String fName;
    private Integer age;
}
