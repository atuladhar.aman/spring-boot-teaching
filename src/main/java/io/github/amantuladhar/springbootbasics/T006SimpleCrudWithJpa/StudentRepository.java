package io.github.amantuladhar.springbootbasics.T006SimpleCrudWithJpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Aman Tuladhar
 * @since 2018-01-11
 */
@Repository
interface StudentRepository extends JpaRepository<Student, Long>{
}
