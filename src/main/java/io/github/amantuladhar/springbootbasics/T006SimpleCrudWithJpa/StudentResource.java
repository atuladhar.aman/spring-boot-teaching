package io.github.amantuladhar.springbootbasics.T006SimpleCrudWithJpa;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Aman Tuladhar
 * @since 2018-01-11
 */
@RestController
@RequestMapping("/students/")
class StudentResource {

    private final StudentRepository studentRepository;

    @Autowired
    StudentResource(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @GetMapping
    public List<Student> findAll() {
        return studentRepository.findAll();
    }

    @GetMapping("{id}")
    public Student findById(@PathVariable Long id){
        return studentRepository.findOne(id);
    }

    @PostMapping
    public Student insert(@RequestBody Student student) {
        return studentRepository.save(student);
    }
    @PutMapping("{id}")
    public Student update(@PathVariable Long id, @RequestBody Student student) {
        return studentRepository.save(student);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable Long id) {
        studentRepository.delete(id);
    }
}
