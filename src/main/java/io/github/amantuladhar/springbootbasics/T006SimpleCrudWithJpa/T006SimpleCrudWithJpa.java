package io.github.amantuladhar.springbootbasics.T006SimpleCrudWithJpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class T006SimpleCrudWithJpa {
	
	public static void main(String[] args) {
		SpringApplication.run(T006SimpleCrudWithJpa.class, args);
	}

}
