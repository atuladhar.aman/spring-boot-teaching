package io.github.amantuladhar.springbootbasics.T001SpringBasics;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class T001Bootstrap {

	public static void main(String[] args) {
		SpringApplication.run(T001Bootstrap.class, args);
	}

}
