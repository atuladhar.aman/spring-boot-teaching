package io.github.amantuladhar.springbootbasics.T003SpecificRequestType;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Aman Tuladhar
 * @since 2018-01-10
 */
@RestController
@RequestMapping("/")
public class TestResource {

    @RequestMapping(method = RequestMethod.GET)
    public String helloWorld() {
        return "HELLO WORLD";
    }

    @PostMapping("postMapping")
    public String getMapping() {
        return "HELLO WORLD";
    }
}
