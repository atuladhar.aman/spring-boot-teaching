package io.github.amantuladhar.springbootbasics.T003SpecificRequestType;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class T003SpecificRequestType {
	
	public static void main(String[] args) {
		SpringApplication.run(T003SpecificRequestType.class, args);
	}

}
