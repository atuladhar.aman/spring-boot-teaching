package io.github.amantuladhar.springbootbasics.T002SpringHelloWorld;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class T002HelloWorld {
	
	public static void main(String[] args) {
		SpringApplication.run(T002HelloWorld.class, args);
	}

}
