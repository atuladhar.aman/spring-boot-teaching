package io.github.amantuladhar.springbootbasics.T002SpringHelloWorld;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Aman Tuladhar
 * @since 2018-01-10
 */
@RestController
@RequestMapping("/")
public class HelloWorldResource {

    @RequestMapping
    public String helloWorld() {
        return "HELLO WORLD";
    }
}
