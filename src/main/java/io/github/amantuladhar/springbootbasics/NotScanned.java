package io.github.amantuladhar.springbootbasics;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Aman Tuladhar
 * @since 2018-01-10
 */

// if this class is moved to package / child packages
// from where spring boot application runs
// this will get picked up

// right now this is not picked up by spring-boot
@RestController
@RequestMapping("no-scanned")
public class NotScanned {

    @RequestMapping
    private String helloWorld() {
        return "HELLO WORLD";
    }
}
