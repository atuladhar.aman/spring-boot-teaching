package io.github.amantuladhar.springbootbasics.T005SimpleDblessCrud;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Aman Tuladhar
 * @since 2018-01-11
 */
@RestController
@RequestMapping("/students/")
public class StudentResource {

    private final StudentRepository studentRepository;

    public StudentResource() {
        this.studentRepository = new StudentRespositoryImpl();
    }

    @GetMapping
    public List<Student> findAll() {
        return studentRepository.findAll();
    }

    @GetMapping("{id}")
    public Student findById(@PathVariable Long id){
        return studentRepository.findById(id);
    }

    @PostMapping
    public Student insert(@RequestBody Student student) {
        return studentRepository.insert(student);
    }
    @PutMapping("{id}")
    public Student update(@PathVariable Long id, @RequestBody Student student) {
        return studentRepository.update(student);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable Long id) {
        studentRepository.delete(id);
    }
}
