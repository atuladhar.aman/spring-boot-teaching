package io.github.amantuladhar.springbootbasics.T005SimpleDblessCrud;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Aman Tuladhar
 * @since 2018-01-11
 */
class StudentRespositoryImpl implements StudentRepository {

    private final List<Student> students = new ArrayList<>();

    @Override
    public List<Student> findAll() {
        return new ArrayList<>(students);
    }

    @Override
    public Student findById(Long id) {
        return students.stream()
            .filter(student -> student.getId().equals(id))
            .collect(Collectors.toList())
            .get(0);
    }

    @Override
    public Student update(Student student) {
        final int index = students.indexOf(student);
        students.remove(student);
        students.add(index, student);
        return student;
    }

    @Override
    public Student insert(Student student) {
        students.add(student);
        return student;
    }

    @Override
    public void delete(Long id) {
        final Student byId = findById(id);
        students.remove(byId);
    }
}

public interface StudentRepository {

    List<Student> findAll();

    Student findById(Long id);

    Student update(Student student);

    Student insert(Student student);

    void delete(Long id);
}
