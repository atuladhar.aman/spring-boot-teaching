package io.github.amantuladhar.springbootbasics.T005SimpleDblessCrud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class T005SimpleDblessCrud {
	
	public static void main(String[] args) {
		SpringApplication.run(T005SimpleDblessCrud.class, args);
	}

}
