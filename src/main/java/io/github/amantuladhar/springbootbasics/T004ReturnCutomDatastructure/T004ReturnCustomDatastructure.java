package io.github.amantuladhar.springbootbasics.T004ReturnCutomDatastructure;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class T004ReturnCustomDatastructure {
	
	public static void main(String[] args) {
		SpringApplication.run(T004ReturnCustomDatastructure.class, args);
	}

}
