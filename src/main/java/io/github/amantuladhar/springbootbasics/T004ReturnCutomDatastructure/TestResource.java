package io.github.amantuladhar.springbootbasics.T004ReturnCutomDatastructure;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Aman Tuladhar
 * @since 2018-01-10
 */
@RestController
@RequestMapping("/")
public class TestResource {

    @GetMapping
    public Student helloWorld() {
        return new Student("ASBC", 2);
    }

}
